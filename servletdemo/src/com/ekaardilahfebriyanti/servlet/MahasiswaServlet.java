package com.ekaardilahfebriyanti.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ekaardilahfebriyanti.data.Mahasiswa;

/**
 * Servlet implementation class MahasiswaServlet
 */
@WebServlet("/MahasiswaServlet")
public class MahasiswaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MahasiswaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//step 1 : set content type
//				response.setContentType("text/html");
				
		//step 2 make printwriter's object
//				PrintWriter out = response.getWriter();
				
		//step 3 make html
//				out.println("Nama : "+request.getParameter("firstname")+" "+request.getParameter("lastname"));
		
		
		
		ArrayList<Mahasiswa> listMhs = new ArrayList<Mahasiswa>();
		listMhs.add(new Mahasiswa("Eka Ardilah", "Febriyanti"));
		listMhs.add(new Mahasiswa("Bumi", "Alana"));
		
		
		
		//step 1 add data in the request
		request.setAttribute("mahasiswa", listMhs);
		
		//step 2 dispatcher
		RequestDispatcher dispatcher = request.getRequestDispatcher("/view_mhs2.jsp");
		
		//step 3 forward data
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//		doGet(request, response);
		// atau
		//step 1 : set content type
		response.setContentType("text/html");
		
		//step 2 make printwriter's object
				PrintWriter out = response.getWriter();
				
		//step 3 make html
				out.println("Nama : "+request.getParameter("firstname")+" "+request.getParameter("lastname"));
	}

}
